package iofunc

import (
	"os"
	"io/ioutil"
)

type SubDir struct {
	Name string
	Entry map[string]int
}

func DirExist(path string) bool {
	var (
		fi os.FileInfo = nil
		err error
	)
	fi, err = os.Lstat(path)
	if err != nil {
		return os.IsExist(err)
	} else {
		return fi.IsDir()
	}

	panic("func DirExist: Unexpected error.")
}

func DirEntryCount(path string) int {
	var (
		fis []os.FileInfo = nil
		err error
	)
	if DirExist(path) {
		fis, err = ioutil.ReadDir(path)
		if err != nil {
			return -1
		}
		return len(fis)
	} else {
		// Dir Not Exist.
		return -1
	}
}

