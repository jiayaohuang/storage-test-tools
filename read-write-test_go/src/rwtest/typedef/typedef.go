package typedef

import (
	"os"
	"fmt"
	"sync"
	"strconv"
)

// Define struct CfgStruct
type CfgStruct struct {
	// config value, filled by InitConf(), from config.json
	Number map[string]int
	String map[string]string

	// global config value from runtime.
	FIDirTest os.FileInfo
	RunningNum int
	RunningNumLock *sync.Mutex

	// Struct point for status
	Stat Status
}

// Set a value of map
// Map may be Number or String
func (this *CfgStruct) Set(key string, value string) int {
	var (
		tmpInt int = 0
		err error = nil
	)
	tmpInt, err = strconv.Atoi(value)
	if err == nil {
		this.Number[key] = tmpInt
	} else {
		this.String[key] = value
	}

	return 0
}

// Get a value of map
// Always return string. So if you want a Nember with int, do not use this.
func (this *CfgStruct) GetToStr(key string) string {
	var (
		value string = ""
	)
	if this.Number[key] != 0 {
		value = strconv.Itoa(this.Number[key])
		return value
	}
	return this.String[key]
}

// Delete a value by key.
func (this *CfgStruct) Delete(key string) {
	delete(this.Number, key)
	delete(this.String, key)
}

// show all key-value.
func (this *CfgStruct) ShowEnv() {
	var (
		key string = ""
		valueNum int = 0
		valueStr string = ""
	)
	for key, valueNum = range this.Number {
		fmt.Printf("%s\t = %d\n", key, valueNum)
	}
	for key, valueStr = range this.String {
		fmt.Printf("%s\t = %s\n", key, valueStr)
	}
}

