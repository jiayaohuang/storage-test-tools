package main

import (
	"os"
	"fmt"
	"rwtest/typedef"
	"rwtest/readconf"
	"rwtest/iofunc"
)

var (
	// type: pointer
	cfg *typedef.CfgStruct	// Mark for abandon
	mapCfg map[string]string = make(map[string]string)
)

func initDir(dir_test string) int {
	var (
		ret_code int = -1
		dir_test_cnt int = 0
		err error = nil
	)
	// init the read-write dir
	if iofunc.DirExist(dir_test) == false {
		err = os.Mkdir(dir_test, 0775)
		if err != nil {
			fmt.Println("Mkdir error:", err.Error())
			return ret_code
		}
	}
	dir_test_cnt = iofunc.DirEntryCount(dir_test)
	if dir_test_cnt < 0 {
		fmt.Println("Read dir error.")
	} else if dir_test_cnt > 0 {
		fmt.Println("Dir", dir_test, "is NOT empty, please use an empty dir instead.")
	} else {
		cfg.FIDirTest, err = os.Lstat(dir_test)
		if err != nil {
			fmt.Println("Get test dir info failed:", err.Error())
		}
		ret_code = 0
	}

	return ret_code
}

func main() {
	var (
		get_code int = 0
	)
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <dir_to_test>\n", os.Args[0])
		os.Exit(1)
	}
	// init "cfg" for config
	cfg, get_code = readconf.InitConf()
	if get_code != 0 {
		fmt.Println("Some error happened while init conf. Exit...")
		os.Exit(1)
	}
	// init dir to test
	if initDir(os.Args[1]) != 0 {
		fmt.Println("Some error happened while init dir. Exit...")
		os.Exit(1)
	}

	// Start process
	FuncMain(cfg)

	return
}

